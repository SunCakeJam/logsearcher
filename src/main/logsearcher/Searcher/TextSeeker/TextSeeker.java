package main.logsearcher.Searcher.TextSeeker;

import main.logsearcher.Match;
import main.logsearcher.SearchResult;
import main.logsearcher.Searcher.EndLineMode;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Seeker of matches in the file.
 * Required set file and seeking text.
 * By default using: UTF-8 and CR LF end line mode.
 */
public class TextSeeker extends AbstractTextSeeker {
    private Charset charset = StandardCharsets.UTF_8;
    private EndLineMode endLineMode = EndLineMode.CR_LF_END_LINE;
    private String text;
    private Path file;

    @Override
    public void setCharset(Charset charset) {
        if (!Charset.availableCharsets().containsValue(charset)) throw new IllegalArgumentException();
        this.charset = charset;
    }
    @Override
    public Charset getCharset() {return charset;}

    public void setText(String text) {this.text = Objects.requireNonNull(text);}
    public String getText() {return text;}

    public void setFile(Path file) {this.file = Objects.requireNonNull(file);}
    public Path getFile() {return file;}

    @Override
    public void setEndLineMode(EndLineMode endLineMode) {this.endLineMode = Objects.requireNonNull(endLineMode);}
    @Override
    public EndLineMode getEndLineMode() {return endLineMode;}

    @Override
    public List<Match> seek() throws IOException {
        List<Match> matches = new LinkedList<>();
        if (text == null || text.isEmpty()) return matches;

        LinkedList<Long> surroundingLinesRAFOffsets = new LinkedList<>();
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            long readed = 0L; //count of readed bytes in Unicode
            long encodingDiff = 0L; //difference of count of bytes in different encoding

            String readedLine;
            while ((readedLine = reader.readLine()) != null) {
                int matchingIndex = readedLine.indexOf(text);
                while (matchingIndex != -1) {
                    Match match = new Match(file, text, matchingIndex, readed + encodingDiff, surroundingLinesRAFOffsets);
                    matches.add(match);

                    matchingIndex = readedLine.indexOf(text, matchingIndex + 1);
                }

                if (surroundingLinesRAFOffsets.size() > SearchResult.MAX_COUNT_OF_LINES) {
                    surroundingLinesRAFOffsets.remove(0);
                }
                surroundingLinesRAFOffsets.add(readed + encodingDiff);

                readed += readedLine.length() + (endLineMode == EndLineMode.CR_LF_END_LINE ? 2 : 1);
                encodingDiff += readedLine.getBytes(charset).length - readedLine.length();
            }
        }
        return matches;
    }
}
