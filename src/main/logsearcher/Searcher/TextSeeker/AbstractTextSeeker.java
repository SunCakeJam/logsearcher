package main.logsearcher.Searcher.TextSeeker;

import main.logsearcher.Match;
import main.logsearcher.Searcher.EndLineMode;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

public abstract class AbstractTextSeeker {

    private Charset charset = StandardCharsets.UTF_8;
    private EndLineMode endLineMode = EndLineMode.CR_END_LINE;

    public abstract List<Match> seek() throws IOException;

    public void setCharset(Charset charset) {this.charset = Objects.requireNonNull(charset);}
    public Charset getCharset() {
        return charset;
    }

    public void setEndLineMode(EndLineMode endLineMode) {this.endLineMode = Objects.requireNonNull(endLineMode);}
    public EndLineMode getEndLineMode() {
        return endLineMode;
    }

}
