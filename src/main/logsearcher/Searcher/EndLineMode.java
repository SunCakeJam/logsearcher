package main.logsearcher.Searcher;

public enum EndLineMode {
    CR_END_LINE("CR"),
    LF_END_LINE("LF"),
    CR_LF_END_LINE("CR LF");

    private String stringImplement;

    EndLineMode(String stringImplement) {
        this.stringImplement = stringImplement;
    }

    @Override
    public String toString() {
        return stringImplement;
    }
}
