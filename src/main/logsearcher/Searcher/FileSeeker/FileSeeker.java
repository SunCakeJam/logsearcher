package main.logsearcher.Searcher.FileSeeker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public class FileSeeker extends AbstractFileSeeker {
    private Path rootPath;
    private String mask;

    public void setMask(String mask) {this.mask = Objects.requireNonNull(mask);}
    public String getMask() { return mask;}

    public void setRootPath(Path rootPath) { this.rootPath = Objects.requireNonNull(rootPath);}
    public Path getRootPath() { return rootPath;}

    @Override
    public List<Path> seek() throws NullPointerException, IOException {
        SearchFileVisitor searchFileVisitor = new SearchFileVisitor(mask);
        Files.walkFileTree(rootPath, searchFileVisitor);
        return searchFileVisitor.getFoundFiles();
    }
}
