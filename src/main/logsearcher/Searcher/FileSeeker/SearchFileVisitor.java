package main.logsearcher.Searcher.FileSeeker;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {

    private String fileExtension;
    private List<Path> foundFiles;

    SearchFileVisitor(String fileExtension) {
        if (fileExtension != null && fileExtension.length() > 0) {
            this.fileExtension = fileExtension.toLowerCase();
        } else {
            this.fileExtension = "^.*\\.log$";
        }
        foundFiles = new LinkedList<>();
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file.getFileName().toString().toLowerCase().matches(fileExtension)) {
            foundFiles.add(file);
        }
        return super.visitFile(file, attrs);
    }

    List<Path> getFoundFiles() {
        return foundFiles;
    }
}