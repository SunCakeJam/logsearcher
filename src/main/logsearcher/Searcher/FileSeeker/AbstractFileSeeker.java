package main.logsearcher.Searcher.FileSeeker;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;

public abstract class AbstractFileSeeker {

    private Path rootPath;
    private String mask;

    abstract public List<Path> seek() throws IOException;

    public void setRootPath(Path rootPath) {this.rootPath = Objects.requireNonNull(rootPath);}
    public Path getRootPath() {return rootPath;}

    public void setMask(String mask) {this.mask = Objects.requireNonNull(mask);}
    public String getMask() {return mask;}
}
