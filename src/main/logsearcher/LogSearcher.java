package main.logsearcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.logsearcher.controllers.Controller;

public class LogSearcher extends Application {

    private static Controller controller;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/logsearcher.fxml"));
        Parent root = loader.load();
        controller = loader.getController();

        primaryStage.setTitle("Log Searcher");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    static Controller getController() {
        return controller;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
