package main.logsearcher;

import main.logsearcher.exceptions.GettingMatchException;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;

public class SearchResult {
    public final static int MAX_COUNT_OF_LINES = 25;

    private HashMap<Path, List<Match>> matchingFiles;
    private HashMap<Path, Integer> currentMatch;
    private int surroundingLines;
    private Charset charset;

    SearchResult(Charset charset) {
        this(charset, 10);
    }

    SearchResult(Charset charset, int surroundingLines) {
        matchingFiles = new LinkedHashMap<>();
        currentMatch = new LinkedHashMap<>();
        this.surroundingLines = surroundingLines;
        this.charset = charset;
    }

    public Map<Path, List<Match>> getMatchingFiles() {
        return Collections.unmodifiableMap(matchingFiles);
    }

    void setSurroundingLines(int surroundingLines) {
        if (surroundingLines < 0) throw new IllegalArgumentException();
        this.surroundingLines = surroundingLines;
    }
    int getSurroundingLines() {
        return surroundingLines;
    }

    private String getMatchRAF(Path file) throws GettingMatchException {
        List<Match> matches = matchingFiles.get(file);
        int matchIndex = currentMatch.get(file);
        if (matches == null || matchIndex < 0 || matchIndex >= matches.size()) throw new GettingMatchException(matchIndex, file);

        Match match = matches.get(matchIndex);
        List<Long> previouslyLines = match.getPreviousLines();
        long startOffset = 0;
        if (surroundingLines == 0) {
            startOffset = match.getLineOffset();
        } else if (previouslyLines.size() > surroundingLines) {
            startOffset = previouslyLines.get(previouslyLines.size() - surroundingLines);
        } else if (previouslyLines.size() > 0) {
            startOffset = previouslyLines.get(0);
        }

        StringBuilder sb = new StringBuilder();
        Charset currentCharset = charset;
        try (RandomAccessFile raf = new RandomAccessFile(file.toFile(), "r")) {
            //Set pointer to starting of surrounding lines.
            raf.seek(startOffset);
            String readedLine;

            //Previously lines
            if (surroundingLines > 0) {
                for (int i = 0; i < surroundingLines && ((readedLine = raf.readLine()) != null); i++) {
                    String unicodeString = new String(readedLine.getBytes(StandardCharsets.ISO_8859_1), currentCharset);
                    sb.append(unicodeString);
                    sb.append('\n');
                }
            }

            //Current line
            if ((readedLine = raf.readLine()) != null) {
                String unicodeString = new String(readedLine.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                sb.append(unicodeString);
                sb.append('\n');
            }

            //Later lines
            if (surroundingLines > 0) {
                for (int i = 0; i < surroundingLines && ((readedLine = raf.readLine()) != null); i++) {
                    String unicodeString = new String(readedLine.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                    sb.append(unicodeString);
                    sb.append('\n');
                }
            }
        } catch (IOException e) {
            throw new GettingMatchException(matchIndex, file);
        }
        return sb.toString();
    }

    public void showMatch(Path file) throws GettingMatchException {
        if (!currentMatch.containsKey(file)) {
            currentMatch.put(file, 0);
        }
        String match = getMatchRAF(file);
        LogSearcher.getController().showFileMatch(file, match);
    }

    public void showPrevMatch(Path file) throws GettingMatchException {
        if (currentMatch.containsKey(file)) {
            int matchIndex = currentMatch.get(file) - 1;
            if (matchIndex < 0) matchIndex = matchingFiles.get(file).size() - 1;
            currentMatch.put(file, matchIndex);
        }
        showMatch(file);
    }

    public void showNextMatch(Path file) throws GettingMatchException {
        if (currentMatch.containsKey(file)) {
            int matchIndex = currentMatch.get(file) + 1;
            if (matchIndex >= matchingFiles.get(file).size()) matchIndex = 0;
            currentMatch.put(file, matchIndex);
        }
        showMatch(file);
    }

    void addMatches(Path file, List<Match> matches) {
        matchingFiles.put(Objects.requireNonNull(file), Objects.requireNonNull(matches));
    }
}
