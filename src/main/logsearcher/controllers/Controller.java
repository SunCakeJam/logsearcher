package main.logsearcher.controllers;

import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.logsearcher.*;
import main.logsearcher.exceptions.GettingMatchException;
import main.logsearcher.exceptions.SearchFileException;
import main.logsearcher.exceptions.SearchTextException;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Controller implements Initializable {

    @FXML
    private GridPane rootGridPane;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField folderField;

    @FXML
    private Button browseButton;

    @FXML
    private TextField extensionField;

    @FXML
    private TextField textField;

    @FXML
    private Button searchButton;

    @FXML
    private TreeView<Path> treeView;

    @FXML
    private Button prevButton;

    @FXML
    private Button nextButton;

    @FXML
    private TabPane tabPane;

    @FXML
    private Button settingsButton;

    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private Label progressLabel;

    @FXML
    private Label endlineModeLabel;

    @FXML
    private Label charsetLabel;

    private HashMap<Path, FileTab> tabs;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        tabs = new HashMap<>();
        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        settingsButton.setGraphic(new ImageView(new Image(LogSearcher.class.getResourceAsStream("images/settings.png"))));
        progressIndicator.setProgress(0);

        SearchService.getInstance().setOnSucceeded(workerStateEvent -> {
            SearchService searchService = SearchService.getInstance();
            SearchResult result = searchService.getSearchResult();
            setTree(searchService.getRootPath(), result.getMatchingFiles().keySet());
            unbindProgress();
            progressLabel.setText("Done");
            searchService.reset();
        });

        SearchService.getInstance().setOnFailed(workerStateEvent -> {
            SearchService searchService = SearchService.getInstance();
            Throwable exception = searchService.getException();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error alert");
            if (!(exception instanceof SearchFileException) && !(exception instanceof SearchTextException)) {
                alert.setHeaderText("Unhandled exception.");

                VBox alertPaneContent = new VBox();

                Label label = new Label("Stack Trace:");

                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                exception.printStackTrace(pw);
                String stackTrace = sw.toString();

                TextArea textArea = new TextArea();
                textArea.setText(stackTrace);

                alertPaneContent.getChildren().addAll(label, textArea);

                // Set content for Dialog Pane
                alert.getDialogPane().setContent(alertPaneContent);

                alert.showAndWait();
                Platform.exit();
            } else {
                alert.setHeaderText(exception.getMessage());
                alert.showAndWait();
            }
            searchService.reset();
            unbindProgress();
            progressLabel.setText("Error");
        });

        SearchService.getInstance().setOnCancelled(workerStateEvent -> {
            SearchService.getInstance().reset();
            unbindProgress();
            treeView.setRoot(null);
            progressLabel.setText("Canceled");
        });

        setCurrentModeLabels();
    }

    private void setCurrentModeLabels() {
        charsetLabel.setText(SearchService.getInstance().getCharset().toString());
        endlineModeLabel.setText(SearchService.getInstance().getEndLineMode().toString());
    }

    private void setTree(Path rootPath, Set<Path> pathSet) {
        Objects.requireNonNull(rootPath);
        Objects.requireNonNull(pathSet);

        TreeItem<Path> rootItem = new TreeItem<>(rootPath, new ImageView(new Image(LogSearcher.class.getResourceAsStream("images/folder.png"))));
        rootItem.setExpanded(true);

        Map<Path, TreeItem<Path>> treeItems = new HashMap<>();
        treeItems.put(rootPath, rootItem);

        for (Path file : pathSet) {
            treeItems.put(file, new TreeItem<>(file.getFileName(), new ImageView(new Image(LogSearcher.class.getResourceAsStream("images/file.png")))));
        }
        for (Path file : pathSet) {
            Path current = file;
            while (true) {
                Path parent = current.getParent();
                if (treeItems.containsKey(parent)) {
                    treeItems.get(parent).getChildren().add(treeItems.get(current));
                    break;
                }
                TreeItem<Path> parentItem = new TreeItem<>(parent.getFileName(), new ImageView(new Image(LogSearcher.class.getResourceAsStream("images/folder.png"))));
                parentItem.setExpanded(true);
                parentItem.getChildren().add(treeItems.get(current));
                treeItems.put(parent, parentItem);
                current = parent;
            }
        }
        treeItems.clear();

        treeView.setRoot(rootItem);
        treeView.refresh();
    }

    @FXML
    void browseButtonOnAction(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        File choosedDirectory = chooser.showDialog(rootGridPane.getScene().getWindow());
        if (choosedDirectory != null) {
            folderField.setText(choosedDirectory.getAbsolutePath());
        }
    }

    @FXML
    void searchButtonOnAction(ActionEvent event) {
        String rootPath = folderField.getText();
        String seekingText = textField.getText();
        String extension = extensionField.getText();

        if (rootPath.isEmpty() || seekingText.isEmpty()) return;
        if (extension.isEmpty()) extension = "*.log";

        treeView.setRoot(null);
        tabs.clear();
        tabPane.getTabs().clear();

        SearchService searchService = SearchService.getInstance();
        if(searchService.getState() != Worker.State.READY) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Start new searching");
            alert.setHeaderText("Search in progress.\nDo you wanna cancel it?");
            alert.setContentText("Start search again after it will canceled.");

            ButtonType cancelButton = new ButtonType("Cancel");
            ButtonType continueButton = new ButtonType("Continue");

            alert.getButtonTypes().clear();
            alert.getButtonTypes().addAll(cancelButton, continueButton);

            Optional<ButtonType> option = alert.showAndWait();
            if (option.get() == cancelButton){
                searchService.cancel();
            }
        } else {
            searchService.setRootPath(rootPath);
            searchService.setSeekingText(seekingText);
            searchService.setMask(extension);
            unbindProgress();
            bindProgress();
            searchService.start();
        }
    }

    @FXML
    void treeViewOnClick(MouseEvent event) {
        if(event.getButton().equals(MouseButton.PRIMARY)){
            if(event.getClickCount() == 2){
                Path selectedPath = getSelectedPath();
                if (selectedPath != null) {
                    try {
                        SearchService.getInstance().getSearchResult().showMatch(selectedPath);
                    } catch (GettingMatchException e) {
                        showGetMatchError(e);
                    }
                }
            }
        }
    }

    @FXML
    void prevButtonOnAction(ActionEvent event) {
        Tab opennedTab = getOpennedTab();
        if (opennedTab instanceof FileTab) {
            try {
                SearchService.getInstance().getSearchResult().showPrevMatch(((FileTab) opennedTab).getFile());
            } catch (GettingMatchException e) {
                showGetMatchError(e);
            }
        }
    }

    @FXML
    void nextButtonOnAction(ActionEvent event) {
        Tab opennedTab = getOpennedTab();
        if (opennedTab instanceof FileTab) {
            try {
                SearchService.getInstance().getSearchResult().showNextMatch(((FileTab) opennedTab).getFile());
            } catch (GettingMatchException e) {
                showGetMatchError(e);
            }
        }
    }

    @FXML
    public void settingButtonOnAction(ActionEvent event) {
        try {
            Stage settings = new Stage();
            FXMLLoader loader = new FXMLLoader(LogSearcher.class.getResource("fxml/settings.fxml"));
            settings.setScene(new Scene(loader.load()));
            settings.initOwner(rootGridPane.getScene().getWindow());
            settings.initModality(Modality.APPLICATION_MODAL);
            settings.showAndWait();
            setCurrentModeLabels();
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Oooops...");
            alert.setHeaderText("Something goes wrong.");
            alert.showAndWait();
        }
    }

    public void showFileMatch(Path file, String match) {
        if (!tabs.containsKey(file)) addFileTab(file);
        setFileTabText(file, match);
        openFileTab(file);
    }

    private Path getSelectedPath() {
        TreeItem selectedItem = treeView.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            StringBuilder fullPath = new StringBuilder();
            fullPath.append(selectedItem.getValue().toString());
            TreeItem currentItem = selectedItem;
            while (currentItem.getParent() != null) {
                currentItem = currentItem.getParent();
                fullPath.insert(0, currentItem.getValue().toString() + "\\");
            }
            Path returnPath = Paths.get(fullPath.toString());
            if (returnPath == null || returnPath.toFile().isDirectory()) return null;
            return returnPath;
        }
        return null;
    }

    private void openFileTab(Path file) {
        if (tabs.containsKey(file)) {
            tabPane.getSelectionModel().select(tabs.get(file));
        }
    }

    private Tab getOpennedTab() {
        return tabPane.getSelectionModel().getSelectedItem();
    }

    private void addFileTab(Path file) {
        if (!tabs.containsKey(file)) {
            FileTab tab = new FileTab(file);
            tab.setOnClosed(event -> {
                Object source = event.getSource();
                if (source instanceof FileTab) {
                    tabs.remove(((FileTab) source).getFile());
                }
            });
            tabPane.getTabs().add(tab);
            tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
            tabs.put(file, tab);
        }
    }

    private void setFileTabText(Path file, String text) {
        if (tabs.containsKey(file)) {
            tabs.get(file).setAreaText(text);
        }
    }

    private void showGetMatchError(GettingMatchException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error alert");
        alert.setHeaderText(e.getMessage());
        alert.showAndWait();
    }

    private void unbindProgress() {
        progressIndicator.progressProperty().unbind();
        progressLabel.textProperty().unbind();
        progressIndicator.setVisible(false);
    }

    private void bindProgress() {
        SearchService searchService = SearchService.getInstance();
        progressIndicator.setVisible(true);
        progressIndicator.progressProperty().bind(searchService.progressProperty());
        progressLabel.textProperty().bind(searchService.messageProperty());
    }

}
