package main.logsearcher.controllers;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import main.logsearcher.Searcher.EndLineMode;
import main.logsearcher.SearchService;

public class SettingsController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<Charset> encodingComboBox;

    @FXML
    private ComboBox<Integer> surroundingLinesComboBox;

    @FXML
    private RadioButton crRadioButton;

    @FXML
    private RadioButton lfRadioButton;

    @FXML
    private RadioButton crlfRadioButton;

    @FXML
    private Label changesLabel;

    @FXML
    private Button okButton;

    @FXML
    void initialize() {
        for (Charset charset: Charset.availableCharsets().values()) {
            encodingComboBox.getItems().add(charset);
        }
        SearchService searchService = SearchService.getInstance();
        encodingComboBox.getSelectionModel().select(searchService.getCharset());

        ToggleGroup toggleGroup = new ToggleGroup();
        crRadioButton.setToggleGroup(toggleGroup);
        lfRadioButton.setToggleGroup(toggleGroup);
        crlfRadioButton.setToggleGroup(toggleGroup);

        switch (searchService.getEndLineMode()) {
            case CR_END_LINE:
                crRadioButton.setSelected(true);
                break;
            case LF_END_LINE:
                lfRadioButton.setSelected(true);
                break;
            case CR_LF_END_LINE:
                crlfRadioButton.setSelected(true);
                break;
        }


        for (int i = 0; i <= 25; i++) {
            surroundingLinesComboBox.getItems().add(i);
        }
        surroundingLinesComboBox.getSelectionModel().select(Integer.valueOf(searchService.getSurroundingLines()));
    }

    @FXML
    void okButtonOnAction(ActionEvent event) {
        SearchService searchService = SearchService.getInstance();
        if (searchService.isRunning()) {
            changesLabel.setTextFill(Color.RED);
            changesLabel.setText("Changes can't be applied.\n Search in progress.");
        } else {
            searchService.setCharset(encodingComboBox.getSelectionModel().getSelectedItem());
            if (crRadioButton.isSelected()) searchService.setEndLineMode(EndLineMode.CR_END_LINE);
            else if (lfRadioButton.isSelected()) searchService.setEndLineMode(EndLineMode.LF_END_LINE);
            else if (crlfRadioButton.isSelected()) searchService.setEndLineMode(EndLineMode.CR_LF_END_LINE);
            else searchService.setEndLineMode(EndLineMode.CR_END_LINE);
            searchService.setSurroundingLines(surroundingLinesComboBox.getSelectionModel().getSelectedItem());
            changesLabel.setTextFill(Color.GREEN);
            changesLabel.setText("Changes applied.");
        }
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(3000), changesLabel);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        fadeTransition.play();
    }
}
