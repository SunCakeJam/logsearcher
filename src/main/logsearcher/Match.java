package main.logsearcher;

import java.nio.file.Path;
import java.util.*;

/**
 * The implementation of the text matches.
 * Contain line offset for Random Access File.
 */
public class Match {

    private Path file;
    private String text;
    private List<Long> previousLines;
    private long offset;
    private long lineOffset;

    public Match(Path file, String text, long offset, long lineOffset, List<Long> previousLines){
        this.file = Objects.requireNonNull(file);
        this.text = Objects.requireNonNull(text);
        this.offset = offset;
        this.lineOffset = lineOffset;
        this.previousLines = new LinkedList<>(Objects.requireNonNull(previousLines));
    }

    public Path getFile() {
        return file;
    }

    public String getText() {
        return text;
    }

    public long getOffset() {
        return offset;
    }

    /**
     * Previously lines - a list of offsets of several lines before the match line.
     */
    List<Long> getPreviousLines() {
        return Collections.unmodifiableList(previousLines);
    }

    long getLineOffset() { return lineOffset;}
}
