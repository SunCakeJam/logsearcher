package main.logsearcher;

import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;

import java.nio.file.Path;

public class FileTab extends Tab {

    private TextArea textArea;
    private Path file;

    public FileTab(Path file) {
        super(file.toString());
        this.file = file;
        textArea = new TextArea();
        textArea.setEditable(false);
        this.setContent(textArea);
        this.setClosable(true);
    }

    public void setAreaText(String text) { textArea.setText(text);}

    public Path getFile() {
        return file;
    }
}
