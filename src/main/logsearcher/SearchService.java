package main.logsearcher;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import Utils.*;
import main.logsearcher.exceptions.SearchFileException;
import main.logsearcher.exceptions.SearchTextException;
import main.logsearcher.Searcher.*;
import main.logsearcher.Searcher.FileSeeker.FileSeeker;
import main.logsearcher.Searcher.TextSeeker.TextSeeker;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

 /**
 * Service for setup and starting searching.
 * Required set root path and wildcard mask.
 */
public class SearchService extends Service<Void> {

    private static volatile SearchService searchService;
    private final static Logger LOGGER = Logger.getLogger(SearchService.class.getName());

    public static SearchService getInstance() {
        if (searchService == null) {
            synchronized (SearchService.class) {
                if (searchService == null)
                    searchService = new SearchService();
            }
        }
        return searchService;
    }

    private FileSeeker fileSeeker;
    private TextSeeker textSeeker;
    private SearchResult searchResult;

    private SearchService() {
        textSeeker = new TextSeeker();
        fileSeeker = new FileSeeker();
        searchResult = new SearchResult(getCharset());
    }

    public SearchResult getSearchResult() {
        return searchResult;
    }

    public void setCharset(Charset charset) {textSeeker.setCharset(charset);}
    public Charset getCharset() { return textSeeker.getCharset();}

    public void setEndLineMode(EndLineMode endLineMode) {textSeeker.setEndLineMode(endLineMode);}
    public EndLineMode getEndLineMode() { return textSeeker.getEndLineMode();}

    public void setSurroundingLines(int surroundingLines) { searchResult.setSurroundingLines(surroundingLines);}
    public int getSurroundingLines() { return searchResult.getSurroundingLines();}

    public void setRootPath(String rootPath) {fileSeeker.setRootPath(Paths.get(rootPath));}
    public Path getRootPath() {return fileSeeker.getRootPath();}

     /**
      * @param mask using wildcard to filtering files.
      */
    public void setMask(String mask) {fileSeeker.setMask(Utils.wildcardToRegex(mask));}
    public void setSeekingText(String seekingText) {textSeeker.setText(seekingText);}

    protected Task<Void> createTask() {
        LOGGER.log(Level.INFO, "New search started:{0}\t" +
                                    "Root:{1}\t" +
                                    "Text:{2}", new String[]{new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()), fileSeeker.getRootPath().toString(), textSeeker.getText()});
        return new Task<>() {
            protected Void call() throws SearchFileException, SearchTextException {
                searchResult = new SearchResult(getCharset(), getSurroundingLines());

                updateMessage("Searching files.");
                List<Path> files;
                if (isCancelled()) {return null;}
                try {
                    files = fileSeeker.seek();
                } catch (NullPointerException | IOException e) {
                    throw new SearchFileException("Error while searching files.\n Search is canceled.");
                }

                updateMessage("Searching text in files.");
                int i = 0, count = files.size();
                updateProgress(0, count);

                for (Path file : files) {
                    try {
                        if (isCancelled()) {return null;}
                        updateMessage("Searching in file " + file);
                        updateProgress(++i, count);
                        textSeeker.setFile(file);
                        List<Match> matches = textSeeker.seek();
                        if (matches != null && matches.size() > 0) {
                            searchResult.addMatches(file, matches);
                        }
                    } catch (IOException e) {
                        throw new SearchTextException("Error with reading file " + file.toString() + "\n Search is canceled.");
                    }
                }
                updateMessage("Done!");
                return null;
            }

            protected void cancelled() {
                super.cancelled();
                updateMessage("Canceled.");
            }
        };
    }
}
