package main.logsearcher.exceptions;

public class SearchFileException extends Exception {
    public SearchFileException(String message) {
        super(message);
    }
}
