package main.logsearcher.exceptions;

public class SearchTextException extends Exception{
    public SearchTextException(String message) {
        super(message);
    }
}
