package main.logsearcher.exceptions;

import java.nio.file.Path;

public class GettingMatchException extends Exception {
    public GettingMatchException(int matchIndex, Path file) {
        super("Can't get match with index: "+ matchIndex +" in file: " + file.toString());
    }
}
